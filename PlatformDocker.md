# Detecting the platform when running a Docker based build

Common advice is to use `uname -m` or `arch` commands which should report something like `x86_64` or `i686` for 64 or 32-bit flavours of Linux running on Intel-esque systems.   This works fine when running on a real machine, but all falls apart when querying on 32 bit docker image running on a 64-bit host, since these commands query the underlying operating system kernel rather than the platform of the container.

A workaround is to look at the system's binaries.  The [Elf format header](https://en.wikipedia.org/wiki/Executable_and_Linkable_Format) specifies that the fourth byte of the header will either be `01` or `02` for a 32 or 64-bit system, so we can look at an assumed-available binary (such as `/bin/bash`) and pull out the relevant information.

----

Imported using `<script>` tag

<script src="https://gitlab.com/benjymous/benjymous.gitlab.io/snippets/1806884.js"></script>

----

Using plugin:

[include](snippets/platform.sh)
