# Summary

* [Introduction](README.md)

* [Build Book](BuildBook.md)
  * [Intro and Prerequisites](Intro.md)
  * [Initial port to Linux](InitialPort.md)
  * [First Steps with CI](CISetup.md)
  * [Porting to other platforms](OtherPlatforms.md)
  * [Portable Linux Binaries](PortableLinux.md)
  * [Windows CI setup](CIWindows.md)
  * [Caching your builds for faster pipelines](Caching.md)
  * [CI for mac](CIMac.md)
  * [CI for other platforms](CIOther.md)
  * [Packaging everything up](Packaging.md)
  * [Pushing to Itch.io](PushItch.md)
* Appendices   
  * [Detecting platform when running on Docker](PlatformDocker.md)


