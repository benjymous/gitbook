# First steps with CI

* Get a single Linux build working in CI
* Add a headless unit tests mode to game
* Enable artefacts from Build step
* Launch tests step from CI
* xvfb allows running graphical stuff on headless systems - creates virtual framebuffer