# Initial Port to Linux

* Set up an Ubuntu VM (or dual-boot)
* Push code from Windows up to GitLab
* Pull code back to Linux
* Create new Linux branch
* Setup CMake scripts
* Fix compile errors
* Push back, build on Windows, fix any additional errors
* Repeat until parity on both platforms
* Merge back to master