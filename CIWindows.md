# Getting GitLab CI builds working for Windows

* run gitlab-runner on a always-on Windows host (e.g. Azure)

* testing
  * Either run on another windows runner (ensure desktop access on the service permissions) or run under Wine on docker
  * Wine makes it easier to run headless