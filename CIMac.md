# Getting GitLab CI builds working for MacOSX

* Assuming no always-on Mac available to do native builds
* Also assuming code does already build natively on OSX
* [OSXCross](https://github.com/tpoechtrager/osxcross) provides a toolchain for building OSX (Darwin) code on linux hosts
* Need recent version of Clang - Debian Stretch ships with Clang 3.8 which isn't new enough, it seems.   Ubuntu 18.04 has Clang 6.x

* some c++ 11 features may not work, or not compile - e.g. std::make_shared seems to be broken, and needs to be worked around