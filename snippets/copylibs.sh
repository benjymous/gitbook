#!/bin/bash

APPNAME=AppName
ARCH=`platform.sh`

echo "output elf format:"
od -An -t x1 -j 4 -N 1 $APPNAME

echo "copying libs to $ARCH"

mkdir $ARCH
yes | cp -i `findlib.sh $APPNAME GLEW` $ARCH
yes | cp -i `findlib.sh $APPNAME libSDL2` $ARCH
yes | cp -i $APPNAME $ARCH/$APPNAME_$ARCH
