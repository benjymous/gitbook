#!/bin/bash

# Name of the binary
APPNAME=AppName

# Get the kernel/architecture information
ARCH=`linuxsh/platform.sh`

if [ -d "$ARCH" ]
then
  echo "Launching $ARCH binaries"
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ARCH
  ./$ARCH/$APPNAME_$ARCH $@
else
  echo "Binaries for arch $ARCH not found!"
  exit 999
fi
