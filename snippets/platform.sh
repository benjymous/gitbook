#!/bin/bash

# hack because docker will always report x86_64 even when running a 32 bit container

# Detect if we're running in a Docker container
# from: https://stackoverflow.com/a/51352271/1073843
is_running_in_container() {
  awk -F: '$3 ~ /^\/$/{ c=1 } END { exit c }' /proc/self/cgroup
}

if is_running_in_container; then
  # figure out bit-ness from a system elf
  # " 01" = 32bit " 02" = 64bit
  # See: https://superuser.com/questions/791506
  BITNESS=`od -An -t x1 -j 4 -N 1 /bin/bash`
  if [ "$BITNESS" == " 01" ]; then  
    echo "i686"
  else
    if [ "$BITNESS" == " 02" ]; then  
      echo "x86_64"
    else
      echo "Unknown"
      exit 999
    fi
  fi
else 
  # Not running inside a container, so use regular uname output
  uname -m
fi
